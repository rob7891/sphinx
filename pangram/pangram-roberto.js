var dictionary = "ABCDEFGHIJKLMNOPQRSTUVXZ";

function isPangram(dictionary, input) {
  var map = {}, counter = dictionary.length;
  for (var i = 0; i < dictionary.length; i++) {
    map[dictionary[i]] = false;
  } // for
  
  for(var i = 0; i < input.length && counter >= 0; i++) {
    var uppercaseLetter = input.charAt(i).toUpperCase();  
    if(map[uppercaseLetter] !== undefined && map[uppercaseLetter] === false) {
      map[uppercaseLetter] = true;
      counter--;
    } // if
  } // for
  return counter > 0 ? 0 : 1;

} // function

console.log(isPangram(dictionary, "abcd"));
console.log(isPangram(dictionary, "dsada asdasd 23423 sasd"));
console.log(isPangram(dictionary, "We promptly judged antique ivory buckles for the next prize"));
console.log(isPangram(dictionary, "We promptly judged antique ivory buckles for the prize"));
